﻿using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MusicBotLib.Utils
{
    public static class EmbedUtils
    {
        public static DiscordEmbedBuilder CreatePlayingEmbed(DiscordUser discordUser, Uri url, string trackName)
        {
            Regex imageRegex = new("https://www.youtube.com/watch\\?v=([a-z0-9A-Z]{11})");
            var match = imageRegex.Match(url.ToString());

            string videoId = match.Groups[1].Value;

            var discordEmbed = new DiscordEmbedBuilder()
                .WithTitle("Now playing")
                .WithDescription(trackName)
                .AddField(url.ToString(), "\u200b")
                .WithImageUrl($"https://img.youtube.com/vi/{videoId}/maxresdefault.jpg")
                .WithFooter($"Requested by: {discordUser.Username}")
                .WithColor(DiscordColor.Red);   
        

            return discordEmbed;
        }

        public static DiscordEmbedBuilder CreateQueuedEmbed(DiscordUser discordUser, Uri url, string trackName, int position)
        {
            Regex imageRegex = new("https://www.youtube.com/watch\\?v=([a-z0-9A-Z]{11})");
            var match = imageRegex.Match(url.ToString());

            string videoId = match.Groups[1].Value;

            var discordEmbed = new DiscordEmbedBuilder()
                .WithTitle("Queued")
                .WithDescription(trackName)
                .AddField(url.ToString(), $"Position in queue: {position}")
                .WithImageUrl($"https://img.youtube.com/vi/{videoId}/maxresdefault.jpg")
                .WithFooter($"Requested by: {discordUser.Username}")
                .WithColor(DiscordColor.Red);


            return discordEmbed;
        }
    }
}
