﻿using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBotLib
{
    public class TrackData
    {
        public LavalinkTrack Track { get; }
        public DiscordUser Requester { get; }

        public TrackData(DiscordUser requester, LavalinkTrack track)
        {
            Track = track;
            Requester = requester;
        }
    }
}
