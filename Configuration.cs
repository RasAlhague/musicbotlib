﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBotLib
{
    public class Configuration
    {
        private static Configuration _instance;

        public string MusicLogFilePath { get; set; }

        protected Configuration()
        {

        }

        public static Configuration GetInstance()
        {
            if (_instance is null)
            {
                string musicLogFilePath = Environment.GetEnvironmentVariable("MUSIC_LOG_FILE");

                _instance = new Configuration
                {
                    MusicLogFilePath = musicLogFilePath
                };
            }

            return _instance;
        }
    }
}
