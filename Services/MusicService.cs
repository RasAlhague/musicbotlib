﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
using DSharpPlus.Lavalink.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MusicBotLib.Utils;

namespace MusicBotLib.Services
{
    public class MusicService : IMusicService
    {
        private bool _handlersCreated;
        public LavalinkState State { get; }

        public MusicService()
        {
            State = new LavalinkState();
            _handlersCreated = false;
        }

        public async Task<bool> TryJoin(CommandContext ctx, DiscordChannel channel)
        {
            var lava = ctx.Client.GetLavalink();
            if (!lava.ConnectedNodes.Any())
            {
                await ctx.RespondAsync("The Lavalink connection is not established");
                return false;
            }

            var node = lava.ConnectedNodes.Values.First();

            if (channel.Type != ChannelType.Voice)
            {
                await ctx.RespondAsync("Not a valid voice channel.");
                return false;
            }


            var conn = await node.ConnectAsync(channel);
            if (!_handlersCreated)
            {
                CreateHandlers(conn);
            }

            return true;
        }

        public async Task<bool> TryLeave(CommandContext ctx)
        {
            var lava = ctx.Client.GetLavalink();
            if (!lava.ConnectedNodes.Any())
            {
                await ctx.RespondAsync("The Lavalink connection is not established");
                return false;
            }

            var node = lava.ConnectedNodes.Values.First();

            if (State.JoinedChannel.Type != ChannelType.Voice)
            {
                await ctx.RespondAsync("Not a valid voice channel.");
                return false;
            }

            var conn = node.GetGuildConnection(State.JoinedChannel.Guild);

            if (conn == null)
            {
                await ctx.RespondAsync("Lavalink is not connected.");
                return false;
            }

            State.Clear();
            State.CurrentTrack = null;
            RemoveHandlers(conn);

            await conn.DisconnectAsync();
            return true;
        }

        public async Task<bool> PlayAsync(CommandContext ctx, Func<LavalinkNodeConnection, Task<LavalinkLoadResult>> searchFunction)
        {
            if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
            {
                await ctx.RespondAsync("You are not in a voice channel.");
                return false;
            }

            var lava = ctx.Client.GetLavalink();
            var node = lava.ConnectedNodes.Values.First();
            var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

            if (conn == null)
            {
                await ctx.RespondAsync("Lavalink is not connected.");
                return false;
            }

            var loadResult = await searchFunction(node);

            if (loadResult.LoadResultType == LavalinkLoadResultType.LoadFailed
                || loadResult.LoadResultType == LavalinkLoadResultType.NoMatches)
            {
                await ctx.RespondAsync($"Track search failed.");
                return false;
            }

            var track = new TrackData(ctx.Member, loadResult.Tracks.First());

            if (conn.CurrentState.CurrentTrack == null)
            {
                State.CurrentTrack = track;
                await conn.PlayAsync(track.Track);

                return true;
            }

            State.Enqueue(track);
            await ctx.RespondAsync(EmbedUtils.CreateQueuedEmbed(ctx.Member, track.Track.Uri, track.Track.Title, State.Count));

            return true;
        }

        public async Task<bool> TryPlay(CommandContext ctx, Uri url)
        {
            if (await PlayAsync(ctx, async (n) => await n.Rest.GetTracksAsync(url)))
            {
                return true;
            }

            return false;
        }

        public async Task<bool> TryPlay(CommandContext ctx, string search)
        {
            if (await PlayAsync(ctx, async (n) => await n.Rest.GetTracksAsync(search, LavalinkSearchType.Youtube)))
            {
                return true;
            }

            return false;
        }

        public async Task<bool> TryPause(CommandContext ctx)
        {
            if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
            {
                await ctx.RespondAsync("You are not in a voice channel.");
                return false;
            }

            var lava = ctx.Client.GetLavalink();
            var node = lava.ConnectedNodes.Values.First();
            var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

            if (conn == null)
            {
                await ctx.RespondAsync("Lavalink is not connected.");
                return false;
            }

            if (conn.CurrentState.CurrentTrack == null)
            {
                await ctx.RespondAsync("There are no tracks loaded.");
                return false;
            }

            await conn.PauseAsync();

            return true;
        }

        public async Task<bool> TryStop(CommandContext ctx, bool withClear = true)
        {
            if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
            {
                await ctx.RespondAsync("You are not in a voice channel.");
                return false;
            }

            var lava = ctx.Client.GetLavalink();
            var node = lava.ConnectedNodes.Values.First();
            var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

            if (conn == null)
            {
                await ctx.RespondAsync("Lavalink is not connected.");
                return false;
            }

            if (conn.CurrentState.CurrentTrack == null)
            {
                await ctx.RespondAsync("There are no tracks loaded.");
                return false;
            }

            if (withClear)
            {
                State.Clear();
            }
            await conn.StopAsync();

            return true;
        }

        public Task<bool> TryLoop(CommandContext ctx)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryLoop(CommandContext ctx, Uri url)
        {
            throw new NotImplementedException();
        }

        public Task<bool> TryLoop(CommandContext ctx, string search)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> TryPlayNext(CommandContext ctx)
        {
            if (!State.TryPeek(out TrackData _))
            {
                await ctx.RespondAsync("The queue is empty!");

                return false;
            }

            if (!await TryStop(ctx, false))
            {
                await ctx.RespondAsync("Cant skip track!");

                return false;
            }

            return true;
        }

        public async Task<bool> TryResume(CommandContext ctx)
        {
            if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
            {
                await ctx.RespondAsync("You are not in a voice channel.");
                return false;
            }

            var lava = ctx.Client.GetLavalink();
            var node = lava.ConnectedNodes.Values.First();
            var conn = node.GetGuildConnection(ctx.Member.VoiceState.Guild);

            if (conn == null)
            {
                await ctx.RespondAsync("Lavalink is not connected.");
                return false;
            }

            if (conn.CurrentState.CurrentTrack == null)
            {
                await ctx.RespondAsync("There are no tracks loaded.");
                return false;
            }

            await conn.ResumeAsync();

            return true;
        }

        public void CreateHandlers(LavalinkGuildConnection conn)
        {
            _handlersCreated = true;
            conn.PlaybackStarted += PlaybackStartedHandler;
            conn.PlaybackFinished += PlaybackFinishedHandler;
        }

        public void RemoveHandlers(LavalinkGuildConnection conn)
        {
            _handlersCreated = false;
            conn.PlaybackStarted -= PlaybackStartedHandler;
            conn.PlaybackFinished -= PlaybackFinishedHandler;
        }

        private async Task PlaybackFinishedHandler(LavalinkGuildConnection sender, TrackFinishEventArgs e)
        {
            if (State.TryDequeue(out TrackData track) && State.JoinedChannel != null)
            {
                State.CurrentTrack = track;

                await sender.PlayAsync(track.Track);
            }
        }

        private async Task PlaybackStartedHandler(LavalinkGuildConnection conn, TrackStartEventArgs e)
        {
            if (State.JoinedChannel == null)
            {
                return;
            }

            await State.LastCommandChannel.SendMessageAsync(EmbedUtils.CreatePlayingEmbed(State.CurrentTrack.Requester, e.Track.Uri, e.Track.Title));
        }
    }
}
