﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBotLib.Services
{
    public interface IMusicService
    {
        LavalinkState State { get; }

        void CreateHandlers(LavalinkGuildConnection conn);
        Task<bool> TryJoin(CommandContext ctx, DiscordChannel channel);
        Task<bool> TryLeave(CommandContext ctx);
        Task<bool> TryPlay(CommandContext ctx, Uri url);
        Task<bool> TryPlay(CommandContext ctx, string search);
        Task<bool> TryPause(CommandContext ctx);
        Task<bool> TryStop(CommandContext ctx, bool withClear = true);
        Task<bool> TryLoop(CommandContext ctx);
        Task<bool> TryLoop(CommandContext ctx, Uri url);
        Task<bool> TryLoop(CommandContext ctx, string search);
        Task<bool> TryPlayNext(CommandContext ctx);
        Task<bool> TryResume(CommandContext ctx);
    }
}
