﻿using DSharpPlus.Entities;
using DSharpPlus.Lavalink;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBotLib
{
    public class LavalinkState
    {
        private readonly ConcurrentQueue<TrackData> _queuedTracks;

        public DiscordChannel JoinedChannel { get; set; }
        public DiscordChannel LastCommandChannel { get; set; }
        public TrackData CurrentTrack { get; set; }
        public int Count { get => _queuedTracks.Count; }

        public LavalinkState()
        {
            _queuedTracks = new();
        }

        public void Enqueue(TrackData track)
        {
            _queuedTracks.Enqueue(track);
        }

        public bool TryDequeue(out TrackData track)
        {
            return _queuedTracks.TryDequeue(out track);
        }

        public bool TryPeek(out TrackData track)
        {
            return _queuedTracks.TryPeek(out track);
        }

        public void Clear()
        {
            _queuedTracks.Clear();
        }

        public bool IsEmpty()
        {
            return _queuedTracks.Count == 0;
        }
    }
}
