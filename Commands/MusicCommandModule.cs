﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Microsoft.Extensions.Logging;
using MusicBotLib.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBotLib.Commands
{
    [Group("music")]
    [Aliases("m")]
    [Description("Commands for using music.")]
    public class MusicCommandModule : BaseCommandModule
    {
        public IMusicService MusicService { get; set; }

        [RequireGuild]
        [Command]
        [Description("Lets the bot join the discord channel. Must be used while joined a voice channel!")]
        public async Task Join(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The join command has been used!");

            if (ctx.Member.VoiceState == null || ctx.Member.VoiceState.Channel == null)
            {
                await ctx.RespondAsync("You are not in a voice channel.");
                return;
            }

            var channel = ctx.Member.VoiceState.Channel;

            await Join(ctx, channel);
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot join the discord channel.")]
        public async Task Join(CommandContext ctx, DiscordChannel channel)
        {
            await LogCommandUsage(ctx, "The join command has been used!");

            if (await MusicService.TryJoin(ctx, channel))
            {
                await ctx.RespondAsync($"Joined {channel.Name}!");
                MusicService.State.JoinedChannel = channel;

                return;
            }

            await ctx.RespondAsync($"Could not join channel: \"{channel.Name}\"!");
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot leave the current joined discord channel.")]
        public async Task Leave(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The leave command has been used!");

            if (await MusicService.TryLeave(ctx))
            {
                await ctx.RespondAsync($"Left {MusicService.State.JoinedChannel.Name}!");
                MusicService.State.JoinedChannel = null;

                return;
            }

            await ctx.RespondAsync($"Could not leave channel: \"{MusicService.State.JoinedChannel.Name}\"!");
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot play the searched track.")]
        public async Task Play(CommandContext ctx, [RemainingText] string search)
        {
            await LogCommandUsage(ctx, "The play command has been used!");

            if (search.StartsWith("https://www.youtube.com/watch?"))
            {
                await MusicService.TryPlay(ctx, new Uri(search));
                return;
            }

            await MusicService.TryPlay(ctx, search);
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot play the searched track.")]
        public async Task Play(CommandContext ctx, Uri url)
        {
            await LogCommandUsage(ctx, "The play command has been used!");

            if (await MusicService.TryPlay(ctx, url))
            {
                ctx.Client.Logger.LogInformation("A track is being played!");
            }
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot pause the currently played track.")]
        public async Task Pause(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The pause command has been used!");

            if (await MusicService.TryPause(ctx))
            {
                await LogAndAnswer(ctx, "The current track has been paused!");
            }
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot stop all playback. The queue will also be emptied!")]
        public async Task Stop(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The playback stop command has been used!");

            if (await MusicService.TryStop(ctx))
            {
                await LogAndAnswer(ctx, "The playback has been stopped.");
            }
        }

        [RequireGuild]
        [Command]
        [Description("Lets the bot resume with the current track. Can not be used after using stop!")]
        public async Task Resume(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "Resume command has been used!");

            if (await MusicService.TryResume(ctx))
            {
                await LogAndAnswer(ctx, "The last track has been resumed!");
            }
        }

        [RequireGuild]
        [Command]
        [Description("Plays the next track in queue!")]
        public async Task Next(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The next command has been used!");

            if (await MusicService.TryPlayNext(ctx))
            {
                ctx.Client.Logger.LogInformation("The next track is being played!");
            }
        }

        [RequireOwner]
        [RequireGuild]
        [Command]
        [Description("Sends the log of the bot.")]
        public async Task Log(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The log command has been used!");

            var configuration = Configuration.GetInstance();

            if (string.IsNullOrEmpty(configuration.MusicLogFilePath))
            {
                await ctx.RespondAsync("No music log file path set!");
                return;
            }

            if(!File.Exists(configuration.MusicLogFilePath))
            {
                await ctx.RespondAsync("File does not exist!");
                return;
            }

            try
            {
                using (var fileStream = new FileStream(configuration.MusicLogFilePath, FileMode.Open))
                {
                    var messageBuilder = new DiscordMessageBuilder()
                        .WithFile("musiccommandlog.csv", fileStream)
                        .WithContent("Here is the log of the music commands!");

                    await messageBuilder.SendAsync(ctx.Channel);
                }
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Could not read command log file!");
            }
        }

        [RequireOwner]
        [RequireGuild]
        [Command]
        [Description("Deletes the log of the bot.")]
        public async Task DeleteLog(CommandContext ctx)
        {
            await LogCommandUsage(ctx, "The log command has been used!");

            var configuration = Configuration.GetInstance();

            if (string.IsNullOrEmpty(configuration.MusicLogFilePath))
            {
                await ctx.RespondAsync("No music log file path set!");
                return;
            }

            if (!File.Exists(configuration.MusicLogFilePath))
            {
                await ctx.RespondAsync("File does not exist!");
                return;
            }

            try
            {
                File.Delete(configuration.MusicLogFilePath);
                await ctx.RespondAsync("Deleted log file!");
            }
            catch (Exception ex)
            {
                ctx.Client.Logger.LogError(ex, "Could not read command log file!");
            }
        }

        private async Task LogCommandUsage(CommandContext ctx, string message)
        {
            ctx.Client.Logger.LogInformation(message);
            MusicService.State.LastCommandChannel = ctx.Channel;

            var config = Configuration.GetInstance();

            if (!string.IsNullOrEmpty(config.MusicLogFilePath))
            {
                try
                {
                    await File.AppendAllTextAsync(config.MusicLogFilePath, $"{DateTime.UtcNow.ToString("dd.MM.yyyy/HH:mm:ss")};{ctx.Guild.Id};{ctx.Member.Id};{ctx.Command.QualifiedName};{ctx.RawArgumentString}\n");
                }
                catch (Exception ex)
                {
                    ctx.Client.Logger.LogError(ex, "Could not write command log file!");
                }
            }
        }

        private async Task LogAndAnswer(CommandContext ctx, string message)
        {
            await ctx.RespondAsync(message);
            ctx.Client.Logger.LogInformation(message);
        }
    }
}
